package com.app.school.management.system.app.schoolapp.db;
import com.app.school.management.system.app.schoolapp.dao.DaoAccess;
import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.model.School;

import androidx.room.Database;
import androidx.room.RoomDatabase;

//room database
@Database(entities = {School.class, SAT.class}, version = 1, exportSchema = false)
public abstract class SchoolDatabase extends RoomDatabase {

    public abstract DaoAccess daoAccess();
}
