package com.app.school.management.system.app.schoolapp.viewmodel;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.app.school.management.system.app.schoolapp.db.SchoolDatabase;
import com.app.school.management.system.app.schoolapp.model.SAT;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.Room;

public class DetailViewModel extends ViewModel {

    private String DB_NAME = "db_school";

    private SchoolDatabase noteDatabase;
    public DetailViewModel(Context context) {
        noteDatabase = Room.databaseBuilder(context, SchoolDatabase.class, DB_NAME).build();
    }
//insert the data in mysql
    public void insertSATData(String name, String reading, String math, String writing) {
        SAT sat = new SAT();
        sat.setName(name);
        sat.setReading(reading);
        sat.setMath(math);
        sat.setWriting(writing);

        insertSAT(sat);
    }
    //insert school details
    public void insertSAT(final SAT sat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Long aLong=noteDatabase.daoAccess().insertSATToDB(sat);
                Log.d("inserTaskcheck",aLong+"ok");
                return null;
            }
        }.execute();
    }
    //get data from dao
    public LiveData<SAT> getSatData(String name) {
        return noteDatabase.daoAccess().getSAT(name);
    }

    public LiveData<List<SAT>> getCount() {
        return noteDatabase.daoAccess().fetchAllSatITems();
    }



}
