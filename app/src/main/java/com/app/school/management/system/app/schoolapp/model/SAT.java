package com.app.school.management.system.app.schoolapp.model;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class SAT implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String reading;
    private String math;
    private String writing;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

    public String getMath() {
        return math;
    }

    public void setMath(String math) {
        this.math = math;
    }

    public String getWriting() {
        return writing;
    }

    public void setWriting(String writing) {
        this.writing = writing;
    }

}
