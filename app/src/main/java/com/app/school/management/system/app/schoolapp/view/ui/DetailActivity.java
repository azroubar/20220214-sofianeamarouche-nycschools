package com.app.school.management.system.app.schoolapp.view.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import android.os.Bundle;
import android.util.Log;

import com.app.school.management.system.app.schoolapp.R;
import com.app.school.management.system.app.schoolapp.databinding.ActivityDetailBinding;
import com.app.school.management.system.app.schoolapp.model.DetailsModel;
import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.model.School;
import com.app.school.management.system.app.schoolapp.viewmodel.DetailViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private ActivityDetailBinding binding;
    private GoogleMap gmap;
    private DetailViewModel dataViewModel;
    private DetailsModel detailsModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_detail);
        dataViewModel = new DetailViewModel(getApplicationContext());
        bindData();
        eventHandling();
        setupMapView(savedInstanceState);

    }

    // this function will get detail sores of given school from database SAT entity
    private void GetSATData(String name) {
        dataViewModel.getSatData(name).observe(this, new Observer<SAT>() {
            @Override
            public void onChanged(@Nullable SAT sat) {
                detailsModel.setSat(sat);
                Log.d("isData","Yes");
                setDataToUI();
            }
        });
    }

    private void setDataToUI() {
        binding.toolbarTittle.setText(detailsModel.getSchool().getName());
        binding.displayName.setText(detailsModel.getSchool().getName());
        binding.displayOverView.setText(detailsModel.getSchool().getOverview());
        binding.displayPhone.setText("Phone: " + detailsModel.getSchool().getPhone());
        binding.displayAddress.setText("Address: " + detailsModel.getSchool().getLocation());
        binding.displayWebsite.setText("website: " + detailsModel.getSchool().getWebsite());

        if(detailsModel.getSat()!=null){
            if(!detailsModel.getSat().getReading().isEmpty() && !detailsModel.getSat().getReading().equals("s")){
                binding.displayReading.setText("SAT Average Critical Reading Score: " + detailsModel.getSat().getReading());
            }
            else{
                binding.displayReading.setText("SAT Average Critical Reading Score: unavailable data");
            }

            if(!detailsModel.getSat().getWriting().isEmpty() && !detailsModel.getSat().getWriting().equals("s")){
                binding.displayWriting.setText("SAT Average Critical Writing Score: " + detailsModel.getSat().getWriting());
            }
            else{
                binding.displayWriting.setText("SAT Average Critical Writing Score: unavailable data");
            }

            if(!detailsModel.getSat().getMath().isEmpty() && !detailsModel.getSat().getMath().equals("s")){
                binding.displayMath.setText("SAT Average Critical Math Score: " + detailsModel.getSat().getMath());
            }
            else{
                binding.displayMath.setText("SAT Average Critical Math Score: unavailable data");
            }
        }
        else{
            binding.displayReading.setText("SAT Average Critical Reading Score: unavailable data");
            binding.displayWriting.setText("SAT Average Critical Writing Score: unavailable data");
            binding.displayMath.setText("SAT Average Critical Math Score: unavailable data");
        }
    }

    // binding UI and data to UI
    void bindData(){
        detailsModel=new DetailsModel();
        School school = (School) getIntent().getSerializableExtra("school");
        detailsModel.setSchool(school);
        GetSATData(school.getName().toUpperCase());
    }
    private void eventHandling(){
        binding.toolbarBack.setOnClickListener(v->{
            onBackPressed();
        });
    }
    private void setupMapView(Bundle savedInstanceState){
        binding.mapView.onCreate(savedInstanceState);

        binding.mapView.getMapAsync(this);

    }

    //show the map nivigation on if latitude and longitude are not null
    // this function will automatically called when map is ready to show navigations
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        String lat = detailsModel.getSchool().getLatitude();
        String lon = detailsModel.getSchool().getLongitude();
        gmap = googleMap;
        gmap.setMinZoomPreference(12);
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setZoomControlsEnabled(true);


        LatLng ny = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));

        //add a marker on the map when the school is
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(ny);
        googleMap.addMarker(markerOptions);

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.mapView.onResume();
    }



    @Override
    protected void onStop() {
        super.onStop();
        binding.mapView.onStop();
    }
    @Override
    protected void onPause() {
        binding.mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        binding.mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapView.onLowMemory();
    }
}