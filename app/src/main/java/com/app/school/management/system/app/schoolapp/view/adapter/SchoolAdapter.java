package com.app.school.management.system.app.schoolapp.view.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.school.management.system.app.schoolapp.model.School;
import com.app.school.management.system.app.schoolapp.R;
import com.app.school.management.system.app.schoolapp.view.ui.DetailActivity;
import com.app.school.management.system.app.schoolapp.view.ui.NavigationActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolHolder> {

    List<School> schoolsArrayList;
    Activity context;

    public SchoolAdapter(List<School> schoolsArrayList, Activity context) {
        this.schoolsArrayList = schoolsArrayList;
        this.context=context;
    }

    @NonNull
    @Override
    public SchoolHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.school_item_row, viewGroup, false);

        SchoolHolder holder = new SchoolHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SchoolHolder holder, @SuppressLint("RecyclerView") final int i) {
        School school= schoolsArrayList.get(i);

        holder.mName.setText(school.getName());
        holder.mAddress.setText(school.getLocation());
        holder.mPhone.setText(school.getPhone());

        holder.mDisplayNavigateAddress.setOnClickListener(v->{
            Intent intent = new Intent( context, NavigationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("latitude" , school.getLatitude());
            intent.putExtra("longitude" , school.getLongitude());
            context.startActivity(intent);
        });

        holder.mPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+school.getPhone()));
                context.startActivity(intent);
            }
        });

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context , DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("school",school);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return schoolsArrayList.size();
    }


    public class SchoolHolder extends RecyclerView.ViewHolder {

        TextView mName,mAddress,mPhone,mDisplayNavigateAddress;
        LinearLayout layout;

        private SchoolHolder(@NonNull View itemView) {
            super(itemView);

            mName = itemView.findViewById(R.id.item_view_name);
            mAddress = itemView.findViewById(R.id.item_view_address);
            mPhone = itemView.findViewById(R.id.item_view_phone);
            mDisplayNavigateAddress = itemView.findViewById(R.id.item_view_navigate_address);
            layout = itemView.findViewById(R.id.layout_holder);
        }
    }
}
