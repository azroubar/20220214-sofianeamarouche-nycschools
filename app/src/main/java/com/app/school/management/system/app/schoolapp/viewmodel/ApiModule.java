package com.app.school.management.system.app.schoolapp.viewmodel;


import com.app.school.management.system.app.schoolapp.model.ApiSchool;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class ApiModule {
    @Provides
    @Singleton
    ApiSchool provideApi() {
        // Make Retrofit API object & return
        return ApiSchool.retrofit.create(ApiSchool.class);
    }
}
