package com.app.school.management.system.app.schoolapp.model;

public class DetailsModel {
    public School school;
    public SAT sat;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public SAT getSat() {
        return sat;
    }

    public void setSat(SAT sat) {
        this.sat = sat;
    }

    @Override
    public String toString() {
        return "DetailsModel{" +
                "school=" + school +
                ", sat=" + sat +
                '}';
    }
}
