package com.app.school.management.system.app.schoolapp.dao;

import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.model.School;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface DaoAccess {

    //insert school object
    @Insert
    Long insertSchool(School school);

    //fetch all school from database
    @Query("SELECT * FROM School")
    LiveData<List<School>> fetchAllSchools();

    //insert school details in the table
    @Insert
    Long insertSATToDB(SAT sat);

    //get specific data from the database
    @Query("SELECT * FROM SAT WHERE name =:school")
    LiveData<SAT> getSAT(String school);


    @Query("SELECT * FROM SAT")
    LiveData<List<SAT>> fetchAllSatITems();


    @Update
    void updateTask(School school);


    @Delete
    void deleteTask(School school);
}
