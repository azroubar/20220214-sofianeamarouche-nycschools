package com.app.school.management.system.app.schoolapp.viewmodel;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.school.management.system.app.schoolapp.db.SchoolDatabase;
import com.app.school.management.system.app.schoolapp.model.School;
import com.app.school.management.system.app.schoolapp.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.room.Room;

public class MainViewModel extends ViewModel {

    private String DB_NAME = "db_school";

    private SchoolDatabase noteDatabase;
    public MainViewModel(Context context) {
        noteDatabase = Room.databaseBuilder(context, SchoolDatabase.class, DB_NAME).build();
    }
    public void insertSchool(String name, String address, String overview, String phone, String latitude, String longitude, String website) {
        School school = new School();
        school.setName(name);
        school.setLocation(address);
        school.setOverview(overview);
        school.setPhone(phone);
        school.setLatitude(latitude);
        school.setLongitude(longitude);
        school.setWebsite(website);


        insertTask(school);
    }


    public void insertTask(final School school) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().insertSchool(school);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final School school) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().deleteTask(school);
                return null;
            }
        }.execute();
    }

    public void updateTask(final School school) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().updateTask(school);
                return null;
            }
        }.execute();
    }

    public LiveData<List<School>> getTasks() {
        return noteDatabase.daoAccess().fetchAllSchools();
    }
}
