package com.app.school.management.system.app.schoolapp;

import com.app.school.management.system.app.schoolapp.model.ApiSchool;
import com.app.school.management.system.app.schoolapp.util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class BaseApiReference {

    // Make Retrofit Api object & return
    public static ApiSchool getApiSchool() {
        // Make Retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.baseURl)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        // Make Retrofit API object
        ApiSchool api = retrofit.create(ApiSchool.class);
        return api;
    }

}
