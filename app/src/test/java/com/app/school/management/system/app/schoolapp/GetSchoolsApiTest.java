package com.app.school.management.system.app.schoolapp;
import com.app.school.management.system.app.schoolapp.model.ApiSchool;
import com.app.school.management.system.app.schoolapp.model.School;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

import static junit.framework.TestCase.assertTrue;
public class GetSchoolsApiTest extends BaseApiReference {
    @Test
    public void GetSchoolsData() {
        ApiSchool api = getApiSchool();
        Call<ArrayList<School>> call = api.getSchoolList();
        try {
            Response<ArrayList<School>> response = call.execute();
            ArrayList<School> schoolSimples = response.body();
            if( schoolSimples == null || schoolSimples.size() == 0 )
                return;
            assertTrue(response.isSuccessful() && schoolSimples.size() > 300 );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
